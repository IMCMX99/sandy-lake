package Metodos;

import java.util.Scanner;

public class InterpolacionLineal {

    public InterpolacionLineal(double[] Intervalos0, double[] Intervalos1) {

        double x0 = Intervalos0[0];
        double fx0 = Intervalos0[1];

        double x1 = Intervalos1[0];
        double fx1 = Intervalos1[1];

        double sup = fx1-fx0;
        double inf = x1-x0;

        double div = sup/inf;

        String fx = Double.toString(fx0) +"+" + Double.toString(div) + "*(x-" + Double.toString(x0) + ")";

        System.out.println("El modelo matematico que pasa por los puntos (\033[34m"+x0+"\033[0m,\033[32m"+fx0+"\033[0m)" +
                " y (\033[34m"+x1+"\033[0m,\033[32m"+fx1+"\033[0m) es aproximadamente: " +
                "\n\033[31m" +fx);
        System.out.println("\033[0m");

        System.out.println("Desea evaluar el modelo en algun valor para x? [s/n]");
        String userO = new Scanner(System.in).nextLine();
        if (userO.equals("s") || userO.equals("S")) {

            System.out.println("Inserte el valor en x");
            String value = new Scanner(System.in).nextLine();
            double val = 0;
            try {

                val = Double.valueOf(value);

            }catch (Exception e) {

                System.out.println("Intente de nuevo");

                try {

                    val = Double.valueOf(value);

                }catch (Exception ec) {

                    System.out.println("Error");
                    System.exit(1);

                }

            }

            System.out.println("La evaluacion del modelo matematico para \033[33m x=" + value + " \033[0m es:"
            +"\n\033[31m" + new Evaluar(fx,"x",val).getResultado() + "\033[0m");

        }

    }
}
