package Metodos;

import Metodos.misc.IteracionVI;

import java.util.ArrayList;

class ValorIntermedio {

    ValorIntermedio(String Funcion, String var, double[] Intervalos, double Tolerancia) {

        double fa = Intervalos[0];
        double fb = Intervalos[1];

        double pn = (Intervalos[0] + Intervalos[1]) / 2;
        ArrayList<IteracionVI> Iteraciones = new ArrayList<>();
        int Iteracion = 1;

        while (true){

            double fn;
            IteracionVI Iter = new IteracionVI();

            if (Iteracion == 1) {

                Iter.setIteracion(Iteracion);
                Iter.setAn(Intervalos[0]);
                Iter.setBn(Intervalos[1]);
                Iter.setPn(pn);
                fn = Math.round(new Evaluar(Funcion, var, pn).getResultado()*1000000d)/1000000d;
                Iter.setFn(fn);
                Iteraciones.add(Iter);

                double Fa = new Evaluar(Funcion,var,Intervalos[0]).getResultado() * fn;

                if ( Fa > 0) {

                    if (fn < 0) {

                        fn = fn *-1.0;
                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fa = pn;

                        }

                    } else {

                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fa = pn;

                        }


                    }

                } else {

                    if (fn < 0) {

                        fn = fn *-1.0;
                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fb = pn;

                        }

                    } else {

                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fb= pn;

                        }


                    }


                }

                Iteracion ++;

            } else {

                Iter.setIteracion(Iteracion);
                Iter.setAn(fa);
                Iter.setBn(fb);

                pn = Math.round(((fa + fb)/2)*1000000d)/1000000d;

                Iter.setPn(pn);
                fn = Math.round(new Evaluar(Funcion, var, pn).getResultado()*1000000d)/1000000d;
                Iter.setFn(fn);
                Iteraciones.add(Iter);

                double Fa = new Evaluar(Funcion,var,fa).getResultado() * fn;

                if ( Fa > 0) {

                    if (fn < 0) {

                        fn = fn *-1.0;
                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fa = pn;

                        }

                    } else {

                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fa = pn;

                        }


                    }

                } else {

                    if (fn < 0) {

                        fn = fn *-1.0;
                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fb = pn;

                        }

                    } else {

                        if(fn<Tolerancia) {

                            break;

                        }else{

                            fb= pn;

                        }


                    }


                }
                Iteracion++;

                if(Iteracion>1000){

                    System.out.println("La Ecuacion En Los Intervalos Seleccionados No Cumple Con Las Condiciones " +
                            "Para Ser Resuelta Por Este Metodo");
                    break;

                }

            }

        }

        if(Iteracion<1000){

            int List = 0;

            for (IteracionVI Wt : Iteraciones) {

                if (List == 0) {

                    System.out.println("\n Iteracion |   an   |   bn   |   pn   |  f(pn) ");
                    System.out.println("\033[32m" + "     " + Wt.getIteracion() + "         "
                            + "\033[34m" + Wt.getAn() + "      " + "\033[33m" + Wt.getBn() + "      " +
                            "\033[36m" + Wt.getPn() + "     " + "\033[35m" + Wt.getFn());
                    List += 1;
                    pn = Wt.getPn();


                } else {

                    System.out.println("\033[32m" + "     " + Wt.getIteracion() + "         "
                            + "\033[34m" + Wt.getAn() + "      " + "\033[33m" + Wt.getBn() + "      " +
                            "\033[36m" + Wt.getPn() + "      " + "\033[35m" + Wt.getFn());
                    List += 1;
                    pn = Wt.getPn();

                }

            }

            System.out.println("\033[31m"+"\nRaiz: " + pn);
            System.out.println("\033[0m");

        }

    }
}
