package Metodos;

import Metodos.misc.IteracionVI;

import java.util.ArrayList;

public class PosicionFalsa {

    public double x0,x, xx, tr, ex, pn;
    public int NIteracion =1;


    public PosicionFalsa(String Funcion, String var, double[] Intervalos, double Tolerancia) {

        boolean Stop = false;
        ArrayList<IteracionVI> Iteraciones = new ArrayList<>();

        while (Stop == false) {

            IteracionVI It = new IteracionVI();

            if (NIteracion == 1){

                x0 = Intervalos[0];
                x = Intervalos[1];
                double fxn1 = new Evaluar(Funcion, var,x).getResultado();
                double fxn = new Evaluar(Funcion, var, x0).getResultado();
                double xx0 = x - x0;

                xx = Math.round((x - ((fxn1*xx0)/(fxn1 - fxn)))*1000000d)/1000000d;
                double fpn = new Evaluar(Funcion, var, xx).getResultado();

                It.setIteracion(NIteracion);
                It.setBn(x);
                It.setAn(x0);
                It.setPn(xx);
                It.setFn(fpn);

                double fn = new Evaluar(Funcion,var,x0).getResultado() * fpn;

                Iteraciones.add(It);

                if (fn > 0){

                    ex = fpn;

                    if (ex<0) {


                        tr = ex * -1.0;

                        if (tr < Tolerancia) {

                            System.out.println(x0);
                            break;

                        } else {

                            if (fpn>0){

                                x = xx;

                            } else if(fpn<0){

                                x0 = xx;

                            }

                        }

                    } else {

                        if (ex < Tolerancia) {

                            System.out.println(x0);
                            break;

                        } else {

                            if (fpn>0){

                                x = xx;

                            } else if(fpn<0){

                                x0 = xx;

                            }

                        }

                    }



                } else {

                    ex = fpn;

                    if (ex<0) {


                        tr = ex * -1.0;

                        if (tr < Tolerancia) {

                            System.out.println(x0);
                            break;

                        } else {

                            if (fpn>0){

                                x = xx;

                            } else if(fpn<0){

                                x0 = xx;

                            }

                        }

                    } else {

                        if (ex < Tolerancia) {

                            System.out.println(x0);
                            break;

                        } else {

                            if (fpn>0){

                                x = xx;

                            } else if(fpn<0){

                                x0 = xx;

                            }

                        }

                    }


                }

                NIteracion++;

            } else {

                double fxn1 = new Evaluar(Funcion, var,x).getResultado();
                double fxn = new Evaluar(Funcion, var, x0).getResultado();
                double xx0 = x - x0;

                xx = Math.round((x - ((fxn1*xx0)/(fxn1 - fxn)))*1000000d)/1000000d;
                double fpn = new Evaluar(Funcion, var, xx).getResultado();

                It.setIteracion(NIteracion);
                It.setBn(x);
                It.setAn(x0);
                It.setPn(xx);
                It.setFn(fpn);

                Iteraciones.add(It);

                ex = fpn;

                if (ex<0) {

                    tr = ex * -1.0;

                    if (tr < Tolerancia) {

                        break;

                    } else {

                        if (fpn>0){

                            x = xx;

                        } else if(fpn<0){

                            x0 = xx;

                        }

                    }

                } else {

                    if (ex < Tolerancia) {

                        break;

                    } else {

                        if (fpn>0){

                            x = xx;

                        } else if(fpn<0){

                            x0 = xx;

                        }

                    }

                }

                NIteracion ++;

                if(NIteracion>1000){

                    System.out.println("La Ecuacion En Los Intervalos Seleccionados No Cumple Con Las Condiciones " +
                            "Para Ser Resuelta Por Este Metodo");
                    break;

                }

            }

        }

        if(NIteracion<1000){

            int List = 0;

            for (IteracionVI Wt : Iteraciones) {

                if (List == 0) {

                    System.out.println("\n Iteracion |   ai   |   bi   |   pi   |  f(pi) ");
                    System.out.println("\033[32m" + "     " + Wt.getIteracion() + "         "
                            + "\033[34m" + Wt.getAn() + "      " + "\033[33m" + Wt.getBn() + "      " +
                            "\033[36m" + Wt.getPn() + "     " + "\033[35m" + Wt.getFn());
                    List += 1;
                    pn = Wt.getPn();


                } else {

                    System.out.println("\033[32m" + "     " + Wt.getIteracion() + "         "
                            + "\033[34m" + Wt.getAn() + "      " + "\033[33m" + Wt.getBn() + "      " +
                            "\033[36m" + Wt.getPn() + "      " + "\033[35m" + Wt.getFn());
                    List += 1;
                    pn = Wt.getPn();

                }

            }

            System.out.println("\033[31m"+"\nRaiz: " + pn +"\033[0m");



        }

    }
}
