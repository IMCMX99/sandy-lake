package Metodos;

import java.util.ArrayList;

public class InterpolacionDeNewton {

    public InterpolacionDeNewton(double[] Points, double[] Evaluations, double Point) {

        double fx0 = Evaluations[0];
        double[] first;
        double[] second;

        ArrayList<double[]> Auxn = new ArrayList<>();

        String aux = "";

        int i = 0;
        int se = 2;

        while ( i < Evaluations.length) {

            if (i == 0) {

                first = new double[Evaluations.length-1];

                for (int j = 1; j < first.length+1; j++) {

                    first[j-1] = (Evaluations[j] - Evaluations[j-1]) / (Points[j] - Points[j-1]);

                }

                Auxn.add(first);
                first = null;
                i++;

            }else{

                first = Auxn.get(i-1);
                second = new double[first.length-1];

                for (int j = 1; j < second.length+1; j++) {

                    second[j-1] = (first[j] - first[j-1]) / (Points[se+j-1] - Points[j-1]);

                }

                Auxn.add(second);
                first = null;
                second = null;
                i++;
                se++;

            }

        }

        String Function = "";

        for (int j = 0; j < Points.length; j++) {

            if (j==0) {

                Function = Double.toString(fx0);

            } else {

                aux += "(x-" + Double.toString(Points[j-1]) +")*";
                Function += "+" + aux + "(" + Double.toString(Auxn.get(j-1)[0]) + ")";

            }

        }

        String fx = Function;

        System.out.println("El polinomio resultante es: " + fx);
        System.out.println("La evaluacion del polinomio en el punto " + Point + " es: " + new Evaluar(fx,"x", Point).getResultado());

    }
}
