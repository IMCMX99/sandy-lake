package Metodos;

import java.util.Scanner;

public class MinimosCuadrados {

    public MinimosCuadrados(double[] Xs, double[] Ys) {

        double[] xy = new double[Xs.length];
        double[] x2 = new double[Xs.length];

        double sumx = 0.0;
        double sumy = 0.0;
        double sumxy = 0.0;
        double sumx2 = 0.0;

        for (int i = 0; i < Xs.length; i++) {

            xy[i] = Xs[i] * Ys[i];
            x2[i] = Xs[i] * Xs[i];

        }

        for (int i = 0; i < Xs.length; i++) {

            sumx += Xs[i];
            sumy += Ys[i];
            sumxy += xy[i];
            sumx2 += x2[i];

        }

        if ( sumx < 0) {

            sumx = sumx * -1;

        }

        double m = (Xs.length*sumxy-(sumx*sumy))/(Xs.length*sumx2-(sumx*sumx));
        double b = (sumy*sumx2-sumx*sumxy)/(Xs.length*sumx2-(sumx*sumx));

        String fx = m + "*x+" + b;
        String fy = "(y-" + b + ")/(" + m + ")";

        System.out.println("El Modelo Matematico Es: " + fx);

        System.out.println("Desea resolver el modelo en algun valor para x o y? (S/N)");

        String userO = new Scanner(System.in).nextLine();
        if (userO.equals("s") || userO.equals("S")) {

            System.out.println("Pose un valor en x o en y? (X/Y)");
            String user1 = new Scanner(System.in).nextLine();
            if (user1.equals("x") || userO.equals("X")) {

                System.out.println("Inserte el valor en x");
                String value = new Scanner(System.in).nextLine();
                double val = 0;
                try {

                    val = Double.valueOf(value);

                }catch (Exception e) {

                    System.out.println("Intente de nuevo");

                    try {

                        val = Double.valueOf(value);

                    }catch (Exception ec) {

                        System.out.println("Error");
                        System.exit(1);

                    }

                }

                System.out.println("La evaluacion del modelo matematico para \033[33m x=" + value + " \033[0m es:"
                        +"\n\033[31m" + new Evaluar(fx,"x",val).getResultado() + "\033[0m");

            } else if (user1.equals("y") || userO.equals("Y")) {

                System.out.println("Inserte el valor en y");
                String value = new Scanner(System.in).nextLine();
                double val = 0;
                try {

                    val = Double.valueOf(value);

                }catch (Exception e) {

                    System.out.println("Intente de nuevo");

                    try {

                        val = Double.valueOf(value);

                    }catch (Exception ec) {

                        System.out.println("Error");
                        System.exit(1);

                    }

                }

                System.out.println("La evaluacion del modelo matematico para \033[33m y=" + value + " \033[0m es:"
                        +"\n\033[31m" + new Evaluar(fy,"y",val).getResultado() + "\033[0m");

            }

        }

    }
}
