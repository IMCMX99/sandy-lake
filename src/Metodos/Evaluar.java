package Metodos;

import org.nfunk.jep.JEP;

public class Evaluar {

    public double Resultado;

    public Evaluar(String Funcion, String variable, double var) {

        JEP j = new JEP();

        j.addStandardConstants();
        j.addStandardFunctions();
        j.addVariable(variable, var);
        j.parseExpression(Funcion);

        this.Resultado = j.getValue();

    }

    public double getResultado() {
        return Resultado;
    }
}
