package Metodos;

public class Tolerancia {

    public double ValueOf = 1, Potence;

    public Tolerancia(String Tolerancia) {

        String[] Notacion = Tolerancia.split("\\^");
        char[] Caracteres = Notacion[1].toCharArray();
        if (Caracteres[0] == '-' ) {

            String value = "";
            for (int i = 1; i < Caracteres.length ; i++) {

                value += Character.toString(Caracteres[i]);

            }

            try {

                this.Potence = - Double.valueOf(value);

            } catch (Exception e) {

                System.out.println("Inserte un numero valido");
                System.exit(1);

            }

        } else {

            String value = Notacion[1];

            try {

                this.Potence = Integer.parseInt(value);

            } catch (Exception e) {

                System.out.println("Inserte un numero valido");
                System.exit(1);

            }

        }

        if (Potence<0) {

            double Valor = 1.0;
            for (int i = -1; i >= Potence; i--) {

                Valor = Valor / 10.0 ;

            }
            this.ValueOf = Valor;

        } else if (Potence>0) {

            double Valor = 1;
            for (int i = 1; i <= Potence; i++) {

                Valor = Valor * 10.0 ;

            }
            this.ValueOf = (double) Valor;

        } else {

            System.out.println("La Tolerancia insertada no existe / no es un valor");

        }

    }

    public double getValueOf() {
        return ValueOf;
    }
}
