package Metodos;

import Metodos.Frames.Airmont;

import java.util.Scanner;

public class Metodos {

    private static boolean Protocol_Methods = true;
    private static boolean Stop = false , useA = false;
    private static double[] Points, Evaluations, Xs, Ys;
    private static int rows;

    public Metodos() {

        String[] args=null;
        main(args);

    }

    public static void main (String[] args){

        int x;

        do{

            if (!Protocol_Methods) {

                System.out.println("\nMetodos Numericos\n\nInserte el numero de la opcion que desea Realizar\n" +
                        "\n1.-Teorema de Taylor\n2.-Teorema del valor Medio \n3.-Salir\n");
                String Option = new Scanner(System.in).nextLine();

                try{

                    x = Integer.parseInt(Option);
                    cases(Protocol_Methods,x);

                } catch (Exception e) {

                    if (Option.equals("active")) {

                        Protocol_Methods = true;
                        System.out.println("\nStarting Emergency Protocol");
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException ex) {
                            e.printStackTrace();
                        }
                        x = 9;
                        cases(Protocol_Methods,x);

                    } else {

                        System.out.println("\nPor Favor Inserte Una Opcion Valida");
                        x = 9;
                        cases(Protocol_Methods,x);

                    }

                }



            } else {

                System.out.println("\nMetodos Numericos\n\nInserte el numero de la opcion que desea Realizar\n" +
                        "\n1.-Teorema de Taylor\n2.-Teorema del Valor Medio\n3.-Metodo de Newton-Raphson" +
                        "\n4.-Metodo de la secante\n5.-Metodo de Posicion Falsa\n6.-Metodo del Punto Fijo\n7.-" +
                        "Interpolacion Lineal\n8.-Interpolacion Cuadratica\n9.-Interpolacion de Newton\n" +
                        "10.-Interpolacion de Lagrange\n11.-Minimos Cuadrados\n12.-Salir\n");
                String Option = new Scanner(System.in).nextLine();

                try{

                    x = Integer.parseInt(Option);
                    cases(Protocol_Methods,x);

                } catch (Exception e) {

                    System.out.println("\nPor Favor Inserte un Numero Valido");
                    x = 9;
                    cases(Protocol_Methods,x);

                }

            }



        }while (!Stop);

    }

    private static int Size(){

        int size = 0;
        System.out.println("Inserte el numero de variables o ecuaciones que posee");
        boolean num = true;
        while(num){

            String Numero = new Scanner(System.in).next();

            try {

                size = Integer.parseInt(Numero);
                num = false;

            }catch (Exception e) {

                System.out.println("Inserte un Numero Valido");
                num = true;

            }

        }
        rows = size;
        return size;

    }

    private static void cases(boolean Protocol, int Option) {

        if (!Protocol) {

            switch (Option){

                case 1:
                    new Taylor(Funcion(1), Var(), Grado(), X0(), Aprox());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    new ValorIntermedio(Funcion(1),Var(),Intervalos(),Tolerancia());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 3:
                    Stop = true;
                case 9:
                    break;
            }

        } else {

            switch (Option){

                case 1:
                    new Taylor(Funcion(1),Var(),Grado(),X0(),Aprox());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    new ValorIntermedio(Funcion(1),Var(),Intervalos(),Tolerancia());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 3:
                    new NewtonRaphson(Funcion(1),Var(),Intervalos(),Tolerancia());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 4:
                    new Secante(Funcion(1), Var(), Intervalos(), Tolerancia());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 5:
                    new PosicionFalsa(Funcion(1),Var(),Intervalos(),Tolerancia());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 6:
                    new PuntoFijo(Funcion(2), Var(), Intervalos(), Tolerancia());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 7:
                    new InterpolacionLineal(points0(),points1());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 8:
                    new InterpolacionCuadratica(points0(),points1(),points2());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 9:
                    new InterpolacionDeNewton(Points(),Evaluations(),Point());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 10:
                    new InterpolaciondeLagrange(Points(),Evaluations());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 11:
                    new MinimosCuadrados(Pointsx(),Pointsy());
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 12:
                    new Airmont("Metodo de Jacobi",Size(),rows,Aproximations(),useA);
                case 13:
                    new Airmont("Eliminacion de Gauss",Size(),rows,Aproximations(),useA);
                case 14:
                    Stop = true;
                    break;
            }

        }

    }

    private static double[] Aproximations() {

        double[] Aprox = new double[rows];
        System.out.println("Tiene Aproximaciones Iniciales?");
        System.out.println("De No Ser Asi Las Variables se Inicializaran en 0");
        System.out.println("Esta De Acuerdo?(S/N)");
        String Opt = new Scanner(System.in).next();
        if(Opt.equals("S") || Opt.equals("s")){

            useA = true;

            for (int i = 0; i < Aprox.length; i++) {

                System.out.println("Inserte el valor de x" + (i+1) +":");
                boolean num = true;
                while(num){

                    String Numero = new Scanner(System.in).next();

                    try {

                        Aprox[i] = Double.parseDouble(Numero);
                        num = false;

                    }catch (Exception e) {

                        System.out.println("Inserte un Numero Valido");
                        num = true;

                    }

                }

            }

        }

        return Aprox;

    }

    private static double[] Intervalos(){

        double[] Inter = new double[2];
        System.out.println("Inserta Los Intervalos a Evaluar");
        System.out.println("Intervalo a");
        Inter[0] = new Scanner(System.in).nextDouble();
        System.out.println("Intervalo b");
        Inter[1] = new Scanner(System.in).nextDouble();

        return Inter;

    }

    private static String Funcion(int opt) {

        switch (opt){

            case 1:
                System.out.println("Inserta la Ecuacion (Dec)");
                return new Scanner(System.in).nextLine();
            case 2:
                System.out.println("Desea calcular alguna derivada?[s/n]");
                if(new Scanner(System.in).nextLine().equals("s")){

                    System.out.println("Inserte la ecuacion");

                    String Funciono = new Derivar(new Scanner(System.in).nextLine()).getDerivada();
                    System.out.println(Funciono);

                }
                System.out.println("Inserta la Ecuacion Despejada (Dec)");
                return new Scanner(System.in).nextLine();
            default:
                break;

        }

        return null;

    }

    private static int Grado() {

        System.out.println("Inserte el grado de Polinomio de Taylor a Usar");
        return new Scanner(System.in).nextInt();

    }

    private static String Var() {

        boolean Continue = true;
        String Var;

        do {

            System.out.println("Inserte la variable que desea operar");
            Var = new Scanner(System.in).nextLine();
            char[] var = Var.toCharArray();

            if (var.length==1) {

                Var = Character.toString( var[0] );
                Continue = false;

            }

        }while (Continue);

        return Var;


    }

    private static double Tolerancia() {

        System.out.println("Inserte la Tolerancia con el formato 10^n");
        String Tolerancia = new Scanner(System.in).nextLine();
        return new Tolerancia(Tolerancia).getValueOf();

    }

    private static double X0() {

        double X0;
        System.out.println("Inserte x0");
        String Value = new Scanner(System.in).nextLine();
        try {

            X0 = Double.valueOf(Value);

        } catch (Exception e) {

            System.out.println("Inserte x0");
            Value = new Scanner(System.in).nextLine();
            X0 = Double.valueOf(Value);

        }

        return X0;

    }

    private static double Aprox() {

        double X0;
        System.out.println("Inserte el valor de x al que se desea aproximar");
        String Value = new Scanner(System.in).nextLine();
        try {

            X0 = Double.valueOf(Value);

        } catch (Exception e) {

            System.out.println("Inserte el valor de x al que se desea aproximar");
            Value = new Scanner(System.in).nextLine();
            X0 = Double.valueOf(Value);

        }

        return X0;

    }

    private static double[] points0(){

        boolean num;
        double[] Inter = new double[2];
        System.out.println("Inserta el primer par de puntos");
        System.out.println("x0");
        num = true;
        Number(num, Inter);
        System.out.println("f(x0)");
        num = true;
        isNumber(num, Inter);


        return Inter;

    }

    private static double[] points1(){

        boolean num;
        double[] Inter = new double[2];
        System.out.println("Inserta el segundo par de puntos");
        System.out.println("x1");
        num = true;
        Number(num, Inter);
        System.out.println("f(x1)");
        num = true;
        isNumber(num, Inter);


        return Inter;

    }

    private static void isNumber(boolean num, double[] inter) {
        while(num){

            String Numero = new Scanner(System.in).next();

            try {

                inter[1]= Double.parseDouble(Numero);
                num = false;

            }catch (Exception e) {

                System.out.println("Inserte un Numero Valido");
                num = true;

            }

        }
    }

    private static double[] points2(){

        boolean num;
        double[] Inter = new double[2];
        System.out.println("Inserta el tercer par de puntos");
        System.out.println("x2");
        num = true;
        Number(num, Inter);
        System.out.println("f(x2)");
        num = true;
        isNumber(num, Inter);

        return Inter;

    }

    private static void Number(boolean num, double[] inter) {
        while(num){

            String Numero = new Scanner(System.in).next();

            try {

                inter[0]= Double.parseDouble(Numero);
                num = false;

            }catch (Exception e) {

                System.out.println("Inserte un Numero Valido");
                num = true;

            }

        }
    }

    private static double[] Evaluations() {

        return Evaluations;

    }

    private static double[] Points(){

        System.out.println("Inserte la cantidad de puntos que posee: ");
        boolean num = true;
        int nu = 0;

        while(num){

            String Numero = new Scanner(System.in).next();

            try {

                nu = Integer.parseInt(Numero);
                num = false;

            }catch (Exception e) {

                System.out.println("Inserte un Numero Valido");
                num = true;

            }

        }

        Points = new double[nu];
        Evaluations = new double[nu];

        if( nu > 0 ) {

            for (int i = 0; i < nu; i++) {

                System.out.println("Inserte el par de puntos numero " + (i+1));
                System.out.println("x");
                num = isNum(num, i, Points);
                System.out.println("fx");
                num = isNum(num, i, Evaluations);

            }

        }

        return Points;

    }

    private static boolean isNum(boolean num, int i, double[] evaluations) {
        num = true;
        while(num){

            String Numero = new Scanner(System.in).next();

            try {

                evaluations[i]= Double.parseDouble(Numero);
                num = false;

            }catch (Exception e) {

                System.out.println("Inserte un Numero Valido");
                num = true;

            }

        }
        return num;
    }

    private static double Point(){

        System.out.println("Inserte el Punto a Evaluar");

        boolean num = true;
        double nu = 0;

        while(num){

            String Numero = new Scanner(System.in).next();

            try {

                nu = Double.parseDouble(Numero);
                num = false;

            }catch (Exception e) {

                System.out.println("Inserte un Numero Valido");
                num = true;

            }

        }

        return nu;

    }

    private static double[] Pointsx() {

        System.out.println("Inserte la cantidad de pares ordenados que posee: ");
        boolean num = true;
        int size=0;
        while(num){

            String Numero = new Scanner(System.in).next();

            try {

                size= Integer.parseInt(Numero);
                num = false;

            }catch (Exception e) {

                System.out.println("Inserte un Numero Valido");
                num = true;

            }

        }

        num = true;

        Xs = new double[size];
        Ys = new double[size];

        for (int i = 0; i < size; i++) {

            System.out.println("Inserte el valor en x numero " + (i+1));
            num = isNum2(num, i, Xs);

            System.out.println("Inserte el valor en y numero " + (i+1));
            num = isNum2(num, i, Ys);

        }

        return Xs;

    }

    private static double[] Pointsy(){

        return Ys;

    }

    private static boolean isNum2(boolean num, int i, double[] xs) {
        num = true;
        while(num){

            String Numero = new Scanner(System.in).next();

            try {
                xs[i]= Integer.parseInt(Numero);
                num = false;

            }catch (Exception e) {

                System.out.println("Inserte un Numero Valido");
                num = true;


            }

        }
        return num;
    }

}
