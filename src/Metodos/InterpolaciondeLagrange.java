package Metodos;

import java.util.Scanner;

public class InterpolaciondeLagrange {

    public InterpolaciondeLagrange(double[] Points, double[] Evaluations) {

        StringBuilder fx = new StringBuilder();
        boolean pos;
        int xi = 0;
        int xj = 0;

        for (int i = 0; i < Points.length; i++) {

            fx.append("((");
            pos = i  == Points.length-1;

            for (int j = 0; j < Points.length; j++) {

                if( i != j ) {

                    if (Points.length>1){

                        if (j==Points.length-1) {

                            fx.append("(x-").append(Points[xj]).append(")");

                        }else if(j<Points.length) {

                            if (pos && (j==i-1)) {

                                fx.append("(x-").append(Points[xj]).append(")");

                            }else{

                                fx.append("(x-").append(Points[xj]).append(")*");

                            }

                        }

                    } else {

                        fx.append("(x-").append(Points[xj]).append(")");

                    }

                }else{



                }

                xj++;

            }

            xj = 0;

            for (int j = 0; j < Points.length; j++) {

                if ( i != j ){

                    if (Points.length>2){

                        if(xi==0 || j==0){

                            fx.append(")/((").append(Double.toString(Points[i])).append("-").append(Points[xj]).append(")*");

                        } else if(j==Points.length-1) {

                            fx.append("(").append(Double.toString(Points[i])).append("-").append(Points[xj]).append(")");

                        } else {

                            if(pos && (j==i-1)) {

                                fx.append("(").append(Double.toString(Points[i])).append("-").append(Points[xj]).append(")");

                            } else {

                                fx.append("(").append(Double.toString(Points[i])).append("-").append(Points[xj]).append(")*");

                            }

                        }

                    }else {

                        fx.append(")/((").append(Double.toString(Points[i])).append("-").append(Points[xj]).append(")");

                    }

                    xi++;

                }else{



                }

                xj++;

            }

            if(i!=Points.length-1){

                fx.append("))*").append(Double.toString(Evaluations[i])).append("+");

            }else{

                fx.append("))*").append(Double.toString(Evaluations[i]));

            }
            xi = 0;
            xj = 0;

        }

        System.out.println("El Modelo Matematico es: " + fx.toString());
        System.out.println("Desea evaluar el modelo en algun valor para x? [s/n]");
        String userO = new Scanner(System.in).nextLine();
        if (userO.equals("s") || userO.equals("S")) {

            System.out.println("Inserte el valor en x");
            String value = new Scanner(System.in).nextLine();
            double val = 0;
            try {

                val = Double.valueOf(value);

            }catch (Exception e) {

                System.out.println("Intente de nuevo");

                try {

                    val = Double.valueOf(value);

                }catch (Exception ec) {

                    System.out.println("Error");
                    System.exit(1);

                }

            }

            System.out.println("La evaluacion del modelo matematico para \033[33m x=" + value + " \033[0m es:"
                    +"\n\033[31m" + new Evaluar(fx.toString(),"x",val).getResultado() + "\033[0m");

        }


    }
}
