package Metodos;

import java.util.Scanner;

public class InterpolacionCuadratica {

    public InterpolacionCuadratica(double[] Intervalos0, double[] Intervalos1, double[] Intervalos2) {

        double x0 = Intervalos0[0];
        double b0 = Intervalos0[1];

        double x1 = Intervalos1[0];
        double fx1 = Intervalos1[1];
        double b1 = (fx1-b0)/(x1-x0);

        double x2 = Intervalos2[0];
        double fx2 = Intervalos2[1];
        double aux0 = (fx2-fx1)/(x2-x1);
        double aux1 = (fx1-b0)/(x1-x0);
        double b2 = (aux0-aux1)/(x2-x0);

        String f2x = Double.toString(b0) + "+" + Double.toString(b1)+"*(x-" + Double.toString(x0) + ")+"
                + Double.toString(b2) + "*(x-" + Double.toString(x0) + ")*(x-" + Double.toString(x1) + ")";

        System.out.println("El modelo matematico que pasa por los puntos (\033[34m"+x0+"\033[0m,\033[32m"+b0+"\033[0m)" +
                " , (\033[34m"+x1+"\033[0m,\033[32m"+fx1+"\033[0m) " +
                "y (\033[34m"+x2+"\033[0m,\033[32m"+fx2+"\033[0m) es aproximadamente: " +
                "\n\033[31m" +f2x);
        System.out.println("\033[0m");

        System.out.println("Desea evaluar el modelo en algun valor para x? [s/n]");
        String userO = new Scanner(System.in).nextLine();
        if (userO.equals("s") || userO.equals("S")) {

            System.out.println("Inserte el valor en x");
            String value = new Scanner(System.in).nextLine();
            double val = 0;
            try {

                val = Double.valueOf(value);

            }catch (Exception e) {

                System.out.println("Intente de nuevo");

                try {

                    val = Double.valueOf(value);

                }catch (Exception ec) {

                    System.out.println("Error");
                    System.exit(1);

                }

            }

            System.out.println("La evaluacion del modelo matematico para \033[33m x=" + value + " \033[0m es:"
                    +"\n\033[31m" + new Evaluar(f2x,"x",val).getResultado() + "\033[0m");

        }


    }
}
