/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos.Frames;

import Metodos.Metodos;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author ivan
 */
public class Airmont extends javax.swing.JFrame {

    public double[][] Matrix = null;
    private DefaultTableModel Model;
    public boolean Status = false, Closed = false;
    int Option,nx;
    double[] XS = null;

    /**
     * Creates new form Window
     * @param Title
     * @param Colums
     * @param Rows
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public Airmont(String Title,int Colums,int Rows,double[] Aproximations, boolean useAprox) {

        super(Title);
        System.out.println("Iniciando Interfaz Grafica...");
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        CreateTable(Colums,Rows);
        if(Title.equals("Metodo de Jacobi")){

            this.Option = 1;

        }
        if(useAprox == true){

            XS = Aproximations;

        }
        nx=Colums;
        setVisible(true);
        
    }

    private void CreateTable(int Colums,int Rows){
      
        int i = 0;
        String x = "x";
        Double[][] MatrixData = new Double[Rows][Colums+2];
        String[] Title = new String[Colums+2];
        
        for (int j = 0; j < Colums+2; j++) {
            
            if(j==Colums){
                
                Title[j] = "=";
                
            } else if(j==Colums+1){
                
                Title[j] = "Numero";
                
            } else {
             
                Title[j] = x + (j+1);
                
            }
            
        }
        
        Model = new DefaultTableModel(MatrixData,Title);
        Table.setModel(Model);
        Table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        Scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        Scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
        for (int j = 0; j < Title.length; j++) {
            
            Table.getColumnModel().getColumn(j).setPreferredWidth(60);
            
        }
        
        Scroll.setViewportView(Table);
        
    }

    private void getData(){

        boolean cont =true;
        this.Matrix = new double[Table.getRowCount()][Table.getColumnCount()-1];

        for (int i = 0; i < Table.getRowCount(); i++) {

            if(cont){

                for (int j = 0; j < Table.getColumnCount(); j++) {

                    if(cont){

                        if(j==Table.getColumnCount()-1){

                            try{

                                Object line = (Object)Table.getValueAt(Table.getRowCount()-1,Table.getColumnCount()-1);
                                String Dec = (String) Table.getValueAt(i,Table.getColumnCount()-1);
                                this.Matrix[i][j-1] = Double.parseDouble(Dec);
                                this.Status = true;

                            }catch(NumberFormatException e){

                                cont = isCont();
                                break;

                            }

                        }else if(j!=Table.getColumnCount()-2){

                             try{

                                String Dec = (String) Table.getValueAt(i,j);
                                this.Matrix[i][j] = Double.parseDouble(Dec);
                                this.Status = true;

                            }catch(NumberFormatException e){

                                 cont = isCont();

                             }

                        }

                    } else {

                        break;

                    }

                }

            } else {

                break;

            }

        }

        this.Closed=true;
        this.setVisible(false);

    }

    private boolean isCont() {
        boolean cont;
        System.out.println("Error Al Procesar datos");
        System.out.println("Tendra que reingresar todos los datos adecuadamente");
        System.out.println("Recuerde, solo se procesaran numeros, NO CARACTERES");
        this.Status = false;
        cont = false;
        return cont;
    }

    private void CalculateValues(){

        ArrayList<Double> AuxXs = new ArrayList<>();
        double[] xs = null;
        double[] oldxs = new double[nx];

        int iter = 0;
        boolean cont = true;

        while(cont){

            xs = new double[nx];

            for (int row = 0; row < Matrix.length; row++) {

                double div = 0;

                for (int column = 0; column < Matrix[row].length; column++) {

                    div = Matrix[row][row]*-1;

                    if( column != row){

                        if(iter!=0){

                            if(column != Matrix[row].length-1){

                                xs[row] += (Matrix[row][column]*oldxs[column])/div;

                            } else {

                                xs[row] += Matrix[row][column]/(div*-1);

                            }

                        } else {

                            if(XS!=null){

                                if(column != Matrix[row].length-1){

                                    xs[row] += (Matrix[row][column]*XS[column])/div;

                                }else{

                                    xs[row] += Matrix[row][column]/(div*-1);

                                }

                            } else {

                                if(column != Matrix[row].length-1){

                                    xs[row] += (Matrix[row][column]*0)/div;

                                }else{

                                    xs[row] += Matrix[row][column]/(div*-1);

                                }

                            }

                        }

                    }

                }

            }

            AuxXs.clear();
            for (int i = 0; i < xs.length; i++) {

                AuxXs.add(xs[i]);

            }

            cont = isCont(xs, oldxs, iter, cont);

            oldxs = null;
            oldxs = new double[xs.length];
            int j = 0;
            for(Double number : AuxXs){

                oldxs[j] = number;
                j++;

            }
            xs = null;
            iter++;

        }

        System.out.println("Calculando...");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        for (int i = 0; i < oldxs.length; i++) {

            System.out.println("El Valor de X"+ (i+1) + " es: " + oldxs[i]);

        }

        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        new Metodos();
        dispose();

    }

    private void CalculateValuesGauss(){

        ArrayList<Double> AuxXs = new ArrayList<>();
        double[] xs = null;
        double[] oldxs = new double[nx];

        int iter = 0;
        boolean cont = true;

        while(cont){

            xs = new double[nx];

            for (int row = 0; row < Matrix.length; row++) {

                double div = 0;

                for (int column = 0; column < Matrix[row].length; column++) {

                    div = Matrix[row][row]*-1;

                    if( column != row){

                        if(iter!=0){

                            if(column != Matrix[row].length-1){

                                xs[row] += (Matrix[row][column]*oldxs[column])/div;

                            } else {

                                xs[row] += Matrix[row][column]/(div*-1);

                            }

                        } else {

                            if(XS!=null){

                                if(column != Matrix[row].length-1){

                                    xs[row] += (Matrix[row][column]*XS[column])/div;

                                }else{

                                    xs[row] += Matrix[row][column]/(div*-1);

                                }

                            } else {

                                if(column != Matrix[row].length-1){

                                    xs[row] += (Matrix[row][column]*xs[column])/div;

                                }else{

                                    xs[row] += Matrix[row][column]/(div*-1);

                                }

                            }

                        }

                    }

                }

            }

            AuxXs.clear();
            for (int i = 0; i < xs.length; i++) {

                AuxXs.add(xs[i]);

            }

            cont = isCont(xs, oldxs, iter, cont);

            oldxs = null;
            oldxs = new double[xs.length];
            int j = 0;
            for(Double number : AuxXs){

                oldxs[j] = number;
                j++;

            }
            xs = null;
            iter++;

        }

        System.out.println("Calculando...");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        for (int i = 0; i < oldxs.length; i++) {

            System.out.println("El Valor de X"+ (i+1) + " es: " + oldxs[i]);

        }

        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        new Metodos();
        dispose();

    }

    private boolean isCont(double[] xs, double[] oldxs, int iter, boolean cont) {
        if(iter!=0){

            boolean isNegative = false;
            boolean isNegative1 = false;
            double[] Auxiliar = new double[nx];

            for (int i = 0; i < Auxiliar.length; i++) {

                Auxiliar[i] = xs[i] - oldxs[i];

            }

            double[] Auxiliar2 = xs;
            Arrays.sort(Auxiliar);
            Arrays.sort(Auxiliar2);

            for (int i = 0; i < Auxiliar.length; i++) {

                if(Auxiliar[i]<0){

                    isNegative = true;

                } else{
                    isNegative = false;
                }

            }

            for (int i = 0; i < Auxiliar2.length; i++) {

                if(Auxiliar2[i]<0){

                    isNegative1 = true;

                } else{
                    isNegative1 = false;
                }

            }

            double sup = 0;
            double inf = 0;

            if(isNegative==true){

                sup = Auxiliar[0];

            }else{
                sup = Auxiliar[Auxiliar.length-1];
            }

            if(isNegative1==true){

                inf = Auxiliar2[0];

            }else {
                inf = Auxiliar2[Auxiliar2.length-1];
            }

            double Condition = sup/inf;

            if (Condition==0){

                cont = false;

            }

        }
        return cont;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        Scroll = new javax.swing.JScrollPane();
        Table = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(60, 63, 65));
        setMinimumSize(new java.awt.Dimension(400, 500));

        jLabel1.setBackground(new java.awt.Color(60, 63, 65));
        jLabel1.setText("Por Favor Inserte Solo Los Coeficientes De Las Ecuaciones");

        Scroll.setBackground(new java.awt.Color(60, 63, 65));

        Table.setModel(new javax.swing.table.DefaultTableModel(
            new JTextField [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            null
        ));
        Scroll.setViewportView(Table);

        jButton1.setText("Insertar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(Scroll, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(jLabel1)
                        .addGap(0, 31, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(170, 170, 170)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(Scroll, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addGap(41, 41, 41))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
        getData();
        if(Option!=0){

            CalculateValues();

        } else {

            CalculateValuesGauss();

        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane Scroll;
    private javax.swing.JTable Table;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
