package Metodos;

import Metodos.misc.Iteracion;
import org.nfunk.jep.JEP;

import java.util.ArrayList;
import java.util.Iterator;

public class NewtonRaphson {

    public int Numero_Iter = 1;
    public double xn;
    public JEP J = new JEP(),Derivada = new JEP();
    boolean Stop = false;
    ArrayList Iter = new ArrayList<Iteracion>();

    public NewtonRaphson(String Funcion, String Var, double[] Intervalos, double Tolerancia) {

        while(Stop == false){

            Iteracion It = new Iteracion();

            if (Numero_Iter == 1){

                double x = (double) (Intervalos[0] + Intervalos[1])/2;

                double FuncO = new Evaluar( Funcion, Var, x ).getResultado();
                double FuncD = new Evaluar( new Derivar(Funcion).getDerivada(), Var, x ).getResultado();

                xn = (double)Math.round( (x - (FuncO/FuncD))* 1000000d )/ 1000000d;

                double FuncR = new Evaluar( Funcion, Var, xn ).getResultado() ;

                if(FuncR < 0){

                    FuncR = FuncR*-1;
                    if (Tolerancia > FuncR) {

                        It.setNumber(Numero_Iter);
                        It.setXn(xn);
                        It.setF(FuncR);
                        Stop = true;

                    } else {

                        It.setNumber(Numero_Iter);
                        It.setXn(xn);
                        It.setF(FuncR);

                    }

                }else{

                    if (Tolerancia > FuncR) {

                        It.setNumber(Numero_Iter);
                        It.setXn(xn);
                        It.setF(FuncR);
                        Stop = true;

                    } else {

                        It.setNumber(Numero_Iter);
                        It.setXn(xn);
                        It.setF(FuncR);

                    }
                }

                Iter.add(It);
                Numero_Iter = 2;

            } else {

                double FuncO = new Evaluar( Funcion, Var, xn ).getResultado();
                double FuncD = new Evaluar( new Derivar(Funcion).getDerivada(), Var, xn ).getResultado();

                xn = (double)Math.round( (xn - (FuncO/FuncD))* 1000000d )/ 1000000d;

                double FuncR = new Evaluar( Funcion, Var, xn ).getResultado() ;

                if(FuncR < 0){

                    FuncR = FuncR*-1;
                    if (Tolerancia > FuncR) {

                        It.setNumber(Numero_Iter);
                        It.setXn(xn);
                        It.setF(FuncR);
                        Stop = true;

                    } else {

                        It.setNumber(Numero_Iter);
                        It.setXn(xn);
                        It.setF(FuncR);

                    }

                }else{

                    if (Tolerancia > FuncR) {

                        It.setNumber(Numero_Iter);
                        It.setXn(xn);
                        It.setF(FuncR);
                        Stop = true;

                    } else {

                        It.setNumber(Numero_Iter);
                        It.setXn(xn);
                        It.setF(FuncR);

                    }
                }

                Iter.add(It);
                Numero_Iter++;

            }

        }

        int List = 0;
        Iterator<Iteracion> Ite = Iter.iterator();

        while (Ite.hasNext()) {

            Iteracion Wt = Ite.next();

            if (List == 0) {

                System.out.println("\n Iteracion |  Valor  | Evaluacion ");
                System.out.println("     " +"\033[34m"+ Wt.getNumber() + "       " + "\033[33m"+Wt.getXn()
                        +"\033[32m"+ "  "+Wt.getF());
                List += 1;

            }else {

                System.out.println("     " + "\033[34m"+Wt.getNumber() + "       " + "\033[33m"+Wt.getXn()
                        + "\033[32m"+"  "+Wt.getF());
                List += 1;

            }

        }

        System.out.println("\033[31m"+"\nRaiz: " + xn);
        System.out.println("\033[0m");

    }
}
