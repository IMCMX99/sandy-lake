package Metodos;

import Metodos.misc.IteracionS;

import java.util.ArrayList;
import java.util.Iterator;

public class Secante {

    public double x0,x, xx, tr, ex;
    public int NIteracion =1;

    public Secante(String Funcion, String Var, double[] Intervalos, double Tolerancia) {

        boolean Stop = false;
        ArrayList Iteraciones = new ArrayList<IteracionS>();

        while (Stop == false) {

            IteracionS It = new IteracionS();

            if (NIteracion == 1){

                x0 = Intervalos[0];
                x = Intervalos[1];
                double fxn1 = new Evaluar(Funcion, Var,x).getResultado();
                double fxn = new Evaluar(Funcion, Var, x0).getResultado();
                double xx0 = x - x0;

                xx = Math.round((x - ((fxn1*xx0)/(fxn1 - fxn)))*1000000d)/1000000d;

                It.setIteracion(NIteracion);
                It.setX(x);
                It.setX0(x0);
                It.setFxn(fxn);

                Iteraciones.add(It);

                ex = fxn;

                if (ex<0) {


                    tr = ex * -1.0;

                    if (tr < Tolerancia) {

                        break;

                    } else {

                        x0 = x;
                        x = xx;

                    }

                } else {

                    if (ex < Tolerancia) {

                        break;

                    } else {

                        x0 = x;
                        x = xx;

                    }

                }

                NIteracion++;

            } else {

                double fxn1 = new Evaluar(Funcion, Var,x).getResultado();
                double fxn = new Evaluar(Funcion, Var, x0).getResultado();
                double xx0 = x - x0;

                xx = Math.round((x - ((fxn1*xx0)/(fxn1 - fxn)))*1000000d)/1000000d;

                It.setIteracion(NIteracion);
                It.setX(x);
                It.setX0(x0);
                It.setFxn(fxn);

                Iteraciones.add(It);

                ex = fxn;

                if (ex<0) {

                    tr = ex * -1.0;

                    if (tr < Tolerancia) {

                        break;

                    } else {

                        x0 = x;
                        x = xx;

                    }

                } else {

                    if (ex < Tolerancia) {

                        break;

                    } else {

                        x0 = x;
                        x = xx;

                    }

                }

                NIteracion ++;

            }

        }

        int List = 0;
        Iterator<IteracionS> Ite = Iteraciones.iterator();

        while (Ite.hasNext()) {

            IteracionS Wt = Ite.next();

            if (List == 0) {

                System.out.println("\n Iteracion |   xn   | f(xn) ");
                System.out.println("     " +"\033[34m"+ Wt.getIteracion() + "         " + "\033[33m"+Wt.getX0()
                        +"\033[32m"+ "     "+Wt.getFxn());
                List += 1;

            }else {

                System.out.println("     " + "\033[34m"+Wt.getIteracion() + "         " + "\033[33m"+Wt.getX0()
                        + "\033[32m"+"     "+Wt.getFxn());
                List += 1;

            }

        }

        System.out.println("\033[31m"+"\nRaiz: " + x0 + "\033[0m");

    }
}
